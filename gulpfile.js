var
    gulp = require('gulp'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    print = require('gulp-print'),
    compressjson = require('./gulp-plugin/gulp-compress-json'),
    cssnano = require('gulp-cssnano'),
    gulpsync = require('gulp-sync')(gulp),
    ejs = require('gulp-ejs-precompiler'),
    handlebars = require('./gulp-plugin/gulp-compile-handlebars'),
    jade = require('gulp-jade'),
    rename = require('gulp-rename'),
    wrap = require('gulp-wrap'),
    webserver = require('gulp-webserver'),
    watch = require('gulp-watch');

gulp.task('clear', function () {
    return del(['./dist/**']);
});

gulp.task('build:js', function () {
    return gulp.src('./src/js/*.js')
        .pipe(print(function (path) {
            return 'build:js => ' + path;
        }))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('build:css', function () {
    return gulp.src('./src/css/*.css')
        .pipe(print(function (path) {
            return 'build:css => ' + path;
        }))
        .pipe(cssnano())
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('build:json', function () {
    return gulp.src('./src/**/*.json')
        .pipe(print(function (path) {
            return 'build:json => ' + path;
        }))
        .pipe(compressjson())
        .pipe(gulp.dest('./dist/'));
});

gulp.task('build:templates:ejs', function () {
    return gulp.src('./src/**/*.ejs')
        .pipe(print(function (path) {
            return 'build:templates:ejs => ' + path;
        }))
        .pipe(ejs({
            compileDebug: true,
            client: true
        }))
        .pipe(wrap('define(["libs/lodash"], function(_) {var templates = {}; return <%= contents %>})'))
        .pipe(uglify())
        .pipe(rename({suffix: '.ejs'}))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('build:templates:handlebars', function () {
    return gulp.src('./src/**/*.handlebars')
        .pipe(print(function (path) {
            return 'build:templates:handlebars => ' + path;
        }))
        .pipe(handlebars())
        .pipe(wrap('define(["libs/handlebars.runtime"], function(Handlebars) {return Handlebars.template(<%= contents %>)})'))
        .pipe(uglify())
        .pipe(rename({suffix: '.handlebars'}))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('build:templates:jade', function () {
    return gulp.src('./src/**/*.jade')
        .pipe(print(function (path) {
            return 'build:templates:jade => ' + path;
        }))
        .pipe(jade({client: true}))
        .pipe(wrap('define(["libs/jade"], function(jade) {return <%= contents %>})'))
        .pipe(uglify())
        .pipe(rename({suffix: '.jade'}))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('build:templates', ['build:templates:ejs', 'build:templates:handlebars', 'build:templates:jade']);

gulp.task('static:index', function () {
    return gulp.src(['./src/index.html', './src/**/*.jpg', './src/**/*.png'])
        .pipe(gulp.dest('./dist/'));
});

gulp.task('static:node_modules', function () {
    return gulp.src([
            './node_modules/requirejs/require.js',
            './node_modules/jquery/dist/jquery.js',
            './node_modules/lodash/lodash.js',
            './node_modules/handlebars/dist/handlebars.runtime.js',
            './node_modules/jade/runtime.js'
        ])
        .pipe(rename(function (path) {
            if (path.basename == 'runtime') {
                path.basename = 'jade'
            }
        }))
        .pipe(gulp.dest('./dist/js/libs/'));
});

gulp.task('build:static', ['static:index', 'static:node_modules']);

gulp.task('build', gulpsync.sync(['clear', ['build:js', 'build:css', 'build:json', 'build:templates', 'build:static']]));

gulp.task('watch', function () {
    gulp.watch(['./src/**', './gulpfile.js', './gulp-plugin/*.js'], ['build']);
});

gulp.task('webserver', ['build', 'watch'], function () {
    gulp.src('./dist/')
        .pipe(webserver({
                livereload: true,
                open: 'index.html',
                directoryListing: true
            })
        )
});

gulp.task('default', ['build']);

/// DEPLOY

gulp.task('deploy:clear', function () {
    return del(['deploy/*', '!deploy/.git/']);
});

gulp.task('deploy:copy', function () {
    return gulp.src(['.deploy-src/*', './dist/**', 'package.json'])
        .pipe(gulp.dest('./deploy/'));
});

gulp.task('deploy', gulpsync.sync(['build', 'deploy:clear', ['deploy:copy']]));