'use strict';
var through = require('through2');
var handlebars = require('handlebars');

var PluginError = require('gulp-util').PluginError;
var ext = require('gulp-util').replaceExtension;

var PLUGIN_NAME = 'gulp-compress-json';

module.exports = function (options) {
    options = options || {};
    var replacer = options.replacer || null, space = options.space;

    function compress(file, encoding, callback) {
        if (file.isNull()) {
            return callback(null, file);
        }

        if (file.isStream()) {
            this.emit('error', new PluginError(PLUGIN_NAME, 'Streams not supported!'));
        } else if (file.isBuffer()) {
            file.contents = new Buffer(handlebars.precompile(String(file.contents)));
            file.path = ext(file.path, '.js');
        }

        callback(null, file);
    }

    return through.obj(compress);
};